import { Router, Request, Response } from 'express';
import { LogInfo } from '../utils/logger';
import { GoodByeResponse } from '../controller/types';
import GoddbyeController from '../controller/GoodbyeController';

// Roueter from express
const goodbyeRouter: Router = Router();

// Define server path for /api/goodbye
goodbyeRouter
  .route('/')
  // http://localhost:8000/api/goodbye?name=Raul/
  .get(async (req: Request, res: Response) => {
    const name: any | undefined = req.query?.name;
    LogInfo(`[api/goodbye]: Query Params -> ${name}`);
    const response: GoodByeResponse = await GoddbyeController.getMessage(name);
    return res.status(200).json(response);
  });

export default goodbyeRouter;
