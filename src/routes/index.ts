import { Router, Request, Response } from 'express';
import helloRouter from './HelloRouter';
import goodbyeRouter from './GoodbyeRouter';
import { LogInfo } from '../utils/logger';

// Router instance
const apiRouter: Router = Router();

// Define server path for /api
apiRouter.get('/', (req: Request, res: Response) => {
  LogInfo('GET: http://localhost:3000/api/');
  res.status(200).send('Welcome to my API RestFul: Express + TS + nodemon + Jest + Swagger + Mongoose');
});

// Define server path for /api/hello
apiRouter.use('/hello', helloRouter);

// Define server path for /api/goodbye
apiRouter.use('/goodbye', goodbyeRouter);

export default apiRouter;
