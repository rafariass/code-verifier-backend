import { Router, Request, Response } from 'express';
import { LogInfo } from '../utils/logger';
import { BasicResponse } from '../controller/types';
import HelloController from '../controller/HelloController';

// Roueter from express
const helloRouter: Router = Router();

// Define server path for /api/hello
helloRouter
  .route('/')
  // http://localhost:8000/api/hello?name=Raul/
  .get(async (req: Request, res: Response) => {
    const name: any | undefined = req?.query?.name;
    LogInfo(`[api/hello]: Query Params -> ${name}`);
    const response: BasicResponse = await HelloController.getMessage(name);
    return res.status(200).send(response);
  });

export default helloRouter;
