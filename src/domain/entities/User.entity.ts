import { Schema, model } from 'mongoose';

export const userEntity = () => {
  const userShema = new Schema({
    name: String,
    email: { type: String, require: true},
    ege: Number
  });

  return model('Users', userShema);
};
