import dotenv from 'dotenv';
import cors from 'cors';
import helmet from 'helmet';
import swaggerUi from 'swagger-ui-express';
import express, { Express, Request, Response } from 'express';
import apiRouter from '../routes';
import { LogError, LogSuccess } from '../utils/logger';

// Configuration .env file
dotenv.config();

// Create Express APP
const app: Express = express();
const port: string | number = process.env.PORT || 3_000;

// Midelware

// Security Config
app.use(cors());
app.use(helmet());

// Content Type Config
app.use(express.urlencoded({ extended: true, limit: '50mb' }));
app.use(express.json({ limit: '50mb' }));

// Static server
app.use(express.static('public'));

// Swagger Config
app.use(
  '/docs',
  swaggerUi.serve,
  swaggerUi.setup(undefined, { swaggerOptions: { url: '/swagger.json', explorer: true } })
);

// Define server path for /api
app.use('/api', apiRouter);

// Redirection Config (http://localhost:8000/ --> http://localhost:8000/api/)
app.all('/', (req: Request, res: Response): void => res.redirect('/api'));

// Execute App and Listen Request to PORT
app.listen(port, (): void => LogSuccess(`[SERVER UP in PORT]: ${port}`));

// Control Server Error
app.on('error', (error): void => LogError(`[SERVER ERROR]: ${error}`));

export default app;
