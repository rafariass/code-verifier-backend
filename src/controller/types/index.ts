/**
 * Basic JSON Response for Controllers
 */
export type BasicResponse = {
  message: string;
};

/**
 * Error JSON Response for Controllers
 */
export type ErrorResponse = {
  error: string;
  message: string;
};

/**
 * GoodBye JSON Response for Controllers
 */
export type GoodByeResponse = {
  message: string;
  date: Date;
};
