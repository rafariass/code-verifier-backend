import { Route, Tags, Get, Query } from 'tsoa';
import { LogSuccess } from '../utils/logger';
import { IGoddbyeController } from './interfaces';
import { GoodByeResponse } from './types';

@Route('/api/goodbye')
@Tags('Goddbye Controller')
class GoddbyeController implements IGoddbyeController {
  /**
   * Endpoint to retrieve a Message "Goddbye {name}" in JSON
   * @param { string | undefined } name Name of user to be "greeted"
   * @returns { Promise<GoodByeResponse> } Promise of GoodByeResponse
   */
  @Get('/')
  public async getMessage(@Query() name?: string): Promise<GoodByeResponse> {
    LogSuccess(`[/api/goodbye] Get Request`);
    return {
      message: `Goodbye ${name || 'world'}!`,
      date: new Date()
      // date: new Date().toLocaleDateString('es-CL', { hour: '2-digit', minute: '2-digit', second: '2-digit' })
    };
  }
}

export default new GoddbyeController();
