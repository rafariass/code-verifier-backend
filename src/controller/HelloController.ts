import { Route, Tags, Get, Query } from 'tsoa';
import { LogSuccess } from '../utils/logger';
import { IHelloController } from './interfaces';
import { BasicResponse } from './types';

@Route('/api/hello')
@Tags('Hello Controller')
class HelloController implements IHelloController {
  /**
   * Endpoint to retrieve a Message "Hello {name}" in JSON
   * @param { string | undefined } name Name of user to be greeted
   * @returns { Promise<BasicResponse> } Promise of BasicResponse
   */
  @Get('/')
  public async getMessage(@Query() name?: string): Promise<BasicResponse> {
    LogSuccess('[/api/hello] Get Request');
    return { message: `Hello ${name || 'World'}!` };
  }
}

export default new HelloController();
