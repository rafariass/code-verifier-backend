import { BasicResponse, GoodByeResponse } from '../types/index';

export interface IHelloController {
  getMessage(name?: string): Promise<BasicResponse>;
}

export interface IGoddbyeController {
  getMessage(name?: string): Promise<GoodByeResponse>;
}
