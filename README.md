
# code-verifier-backend (Open Bootcamp)

Proyecto generado en el Curso de MERN de Open Bootcamp.



## dependencies

| dependencies  | ver     | Descripcion                                                  |
| ------------- | ------- | ------------------------------------------------------------ |
| dotenv | 16.0.3 | Permite utilizar variables de entorno en Node |
| express | 4.18.2 | Framework mas utilizado de node para la creacion de servidores |

## devDependencies

| devDependencies  | ver     | Descripcion                                                  |
| ---------------- | ------- | ------------------------------------------------------------ |
| @types/express | 4.17.17 | Permite utilizar el tipado de TS para Express |
| @types/jest | 29.4.0 | Permite utilizar el tipado de TS para Jest |
| @types/node | 18.13.0 | Permite utilizar el tipado de TS para Node |
| @typescript-eslint/eslint-plugin | 5.51.0 | Permite utilizar el tipado de TS para Eslint |
| concurrently | 7.6.0 | Permite ejecutar mas de un comando al mismo tiempo en la terminal |
| eslint | 8.33.0 | Reglas de estilo para nuestro codigo |
| eslint-config-standard-with-typescript | 34.0.0 | Reglas de estilo para nuestro codigo |
| eslint-plugin-import | 2.27.5 | Reglas de estilo para nuestro codigo |
| eslint-plugin-n | 15.6.1 | Reglas de estilo para nuestro codigo |
| eslint-plugin-promise | 6.1.1 | Reglas de estilo para nuestro codigo |
| jest | 29.4.1 | Permite realizar pruebas unitarias al codigo |
| nodemon | 2.0.20 | Permite escuchar los cambios del codigo y reiniciar el servidor |
| serve | 14.2.0 | Permite generar un servidor |
| supertest | 6.3.3 | Ayudara a la creacion de los test |
| ts-jest | 29.0.5 | Jest con tipado de Typescript |
| ts-node | 10.9.1 | Node con tipado de Typescript |
| typescript | 4.9.5 | Permite generar tipado entre otras cosas al codigo js |
| webpack | 5.75.0 | Empaquetador del proyecto |
| webpack-cli | 5.0.1 | Empaquetador del proyecto |
| webpack-node-externals | 3.0.0 | Empaquetador del proyecto |
| webpack-shell-plugin | 0.5.0 | Empaquetador del proyecto |

## NPM Script

| Script's  | Command     | Descripcion                                                  |
| ------------- | ------- | ------------------------------------------------------------ |
| build | npx tsc | Compila el proyecto |
| start | node dist/index.js | Ejecuta con node el archivo index.js ya compilado |
| dev | concurrently \"npx tsc --watch\" \"npx nodemon -q dist/index.js\" | Compila el proyecto ts a js y ejecuta / observa con nodemon el archivo index.js ya compilado  |
| test | jest | Ejecuta las pruebas unitarias |
| serve:coverage | npm run test && cd coverage/lcov-report && npx serve | Ejecuta las pruebas unitaria, genera un reporte de test y cobertura total del proyecto y lo disponibliza en un servidor |

## environment

| Variable | Value     | Descripcion                                                  |
| ------------- | ------- | ------------------------------------------------------------ |
| PORT | 3000 | Puerto de express |

